# FormantaReact

Collection of React helper.

### Licence

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more information on the Licences which are applied read LICENCE.md

# Made by

Original author is Michael Becker, michael@bemit.codes