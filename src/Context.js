import React from 'react';

/**
 * Creates a HOC for a React Context Consumer, where the result could just be used as any normal HOC
 *
 * @example
 * // create Context and HOC
 * const ThemeContext = React.createContext({});
 *
 * export const withTheme = withContextConsumer(ThemeContext.Consumer);
 *
 * // other file: apply consumer HOC to target Component
 * export const StyledButton = withTheme(Button);
 *
 * @param ContextConsumer a react context consumer, which should made as an consumer HOC
 *
 * @return {function(React.Component|function): function|React.Component}
 */
const withContextConsumer = (ContextConsumer) => (
    (Component) => (
        (props) => {
            return (
                <ContextConsumer>
                    {(context) => <Component {...context} {...props}/>}
                </ContextConsumer>
            )
        }
    )
);

export {withContextConsumer};