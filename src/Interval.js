import React from 'react';

const withInterval = (WrappedComponent) =>
    class extends React.Component {
        intervals = [];

        componentWillUnmount() {
            for(let id in this.intervals) {
                window.clearInterval(id);
                delete this.intervals[id];
            }
        }

        setInterval = (fn, time) => {
            this.intervals[setInterval(fn, time)] = true;
        };

        clearInterval = (id) => {
            if(this.intervals[id]) {
                window.clearInterval(id);
                delete this.intervals[id];
            }
        };

        render() {
            return <WrappedComponent {...this.props} setInterval={this.setInterval} clearInterval={this.clearInterval}>
                {this.props.children}
            </WrappedComponent>
        }
    };

export {withInterval};
