import React from 'react';

const withTimeout = (WrappedComponent) =>
    class extends React.Component {
        timeouts = [];

        componentWillUnmount() {
            for(let id in this.timeouts) {
                window.clearTimeout(id);
                delete this.timeouts[id];
            }
        }

        setTimeout = (fn, delay) => {
            this.timeouts[setTimeout(fn, delay)] = true;
        };

        clearTimeout = (id) => {
            if(this.timeouts[id]) {
                window.clearTimeout(id);
                delete this.timeouts[id];
            }
        };

        render() {
            return <WrappedComponent {...this.props} setTimeout={this.setTimeout} clearTimeout={this.clearTimeout}>
                {this.props.children}
            </WrappedComponent>
        }
    };

export {withTimeout};
